﻿USE Master
go

IF OBJECT_ID ('QuanLySoTietKiem') Is Not Null
	Drop Database QuanLySoTietKiem
go
Create Database QuanLySoTietKiem
go
use QuanLySoTietKiem
go
--------------Tao Table----------
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SoTietKiem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
Create Table SoTietKiem
(
	MaSoTietKiem int IDENTITY(1,1), --Tự tăng
	TenKhachHang nvarchar(50),
	SoCMND nvarchar(50),
	DiaChi nvarchar(255),
	NgayMoSo Date,
	SoTienGuiBanDau int,
	LoaiTietKiem int,
	DongSo bit,
	SoDu int default 0,
	BiXoa bit
	Constraint PK_SoTietKiem
	primary key (MaSoTietKiem)
)
END


---
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LoaiTietKiem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
Create Table LoaiTietKiem
(
	MaLoaiTietKiem int IDENTITY(1,1), --Tự tăng
	TenLoaiTietKiem nvarchar(50),
	LaiSuat float,
	KyHan int,
	SoNgayDuocRut int,
	DangDung bit,
	DuocGuiThem bit,
	BiXoa bit
	Constraint PK_LoaiTietKiem
	primary key(MaLoaiTietKiem)
)
END
go

---

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ThamSo]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
create Table ThamSo
(
	SoTienGuiToiThieu int,
	TienGuiThemToiThieu int,
	LaiSuatThang float,
)
END
go

---

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PhieuGuiTien]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
Create table PhieuGuiTien
(
	MaPhieuGuiTien int IDENTITY(1,1),
	MaSoTietKiem int,
	TenKhachHang nvarchar(50),
	SoTienGui int,
	NgayGui Date,
	BiXoa bit
	Constraint PK_PhieuGuiTien
	primary key(MaPhieuGuiTien)
)
END
go

---

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PhieuRutTien]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
Create table PhieuRutTien
(
	MaPhieuRutTien int IDENTITY(1,1),
	MaSoTietKiem int,
	TenKhachHang nvarchar(50),
	SoTienRut int,
	NgayRut Date,
	BiXoa bit
	Constraint PK_PhieuRutTien
	primary key(MaPhieuRutTien)
)
END
go

-------------Them Khoa Ngoai---------------

alter Table SoTietKiem
add

	Constraint FK_SoTietKiem_LoaiTietKiem
	foreign key (LoaiTietKiem)
	References LoaiTietKiem
alter Table PhieuGuiTien
add
	Constraint FK_PhieuGuiTien_SoTietKiem
	foreign key(MaSoTietKiem)
	References SoTietKiem
alter Table PhieuRutTien
add
	Constraint FK_PhieuRutTien_SoTietKiem
	foreign key(MaSoTietKiem)
	References SoTietKiem
	-----------------------------------------------------------------------------
go
insert [dbo].[LoaiTietKiem]([TenLoaiTietKiem],[LaiSuat],[KyHan],[SoNgayDuocRut],[DangDung],[DuocGuiThem],[BiXoa]) values ( N'3 Tháng', 0.5, 90, 90, 1, 1,0)
insert [dbo].[LoaiTietKiem]([TenLoaiTietKiem],[LaiSuat],[KyHan],[SoNgayDuocRut],[DangDung],[DuocGuiThem],[BiXoa]) values ( N'6 Tháng', 0.55, 180, 180, 1, 1,0)
insert [dbo].[LoaiTietKiem]([TenLoaiTietKiem],[LaiSuat],[KyHan],[SoNgayDuocRut],[DangDung],[DuocGuiThem],[BiXoa]) values ( N'Vô thời hạn', null, null, null, 1, 1,0)
go
insert SoTietKiem(TenKhachHang,SoCMND,DiaChi,SoTienGuiBanDau, SoDu,NgayMoSo,LoaiTietKiem,DongSo, BiXoa)
values
		(N'Dương Khắc Huy','315269748',N'Bính Chánh,TPHCM',35000000,35000000,CAST(N'2018-11-21' AS Date),1,1,0),
		(N'Vương Thanh Hoàng','495687123','Lạc Long Quân,Quận 5,TPHCM',100000000,100000000,CAST(N'2018-09-12' AS Date),3,1,0),
		(N'Lâm Hoàn','796582314','83 Hoàng Hoa Thám,Quận Tân Bình,TPHCM',25000000,25000000,CAST(N'2016-12-09' AS Date),3,1,0),
		(N'Lâm Duy','795687512','85 Hoàng Hoa Thám,Quận Tân Binh,TPHCM',29000000,9000000,CAST(N'2016-12-09' AS Date),2,1,0),
		( N'Nguyễn Huỳnh Thảo Mai', N'824635169', N'312 Bình Hưng Hoà, Bình Tân, TP.HCM', 8500000,8500000,CAST(N'2017-04-18' AS Date) , 3, 1,0),
		( N'Trần Trung Chính', N'932562178', N'40 Nguyễn An Ninh, Đam Bri, Bảo Lộc, Lâm Đồng', 5800000, 5800000, CAST(N'2018-07-19' AS Date), 3, 1,0)



-------------------------------------------------------------------------QUY DỊNH-----------------------------------------------------------
go
--------------Thêm Quy Định 1-------------------
Create trigger trg_QuyDinh1 on SoTietKiem
for insert
as
begin
	if(not exists(select *
				from inserted i join LoaiTietKiem l on i.LoaiTietKiem=l.MaLoaiTietKiem
				where (l.TenLoaiTietKiem like N'%Vô thời hạn%' or l.TenLoaiTietKiem like N'%3 Tháng%'
						or l.TenLoaiTietKiem like N'%6 Tháng%') and i.SoTienGuiBanDau>=100000))
	begin
		print N'Nhập Sai Dữ Liệu'
		rollback tran
	end
	else
	begin
		update SoTietKiem set NgayMoSo=getdate(),SoDu=i.SoTienGuiBanDau
		from inserted i,SoTietKiem s
		where i.MaSoTietKiem=s.MaSoTietKiem
	end
end
go
-------------------------------------
create trigger trg_CheckThongTinKH on SoTietKiem
for insert
as
begin
	if(exists(select *
				from inserted i,SoTietKiem s
				where (i.MaSoTietKiem != s.MaSoTietKiem and i.SoCMND=s.SoCMND)))
	begin
		print N'Khách hàng đã mở sổ trong hệ thống'
		rollback tran
	end
end
go
insert [dbo].[SoTietKiem]( [TenKhachHang], [SoCMND], [DiaChi],  [SoTienGuiBanDau], [LoaiTietKiem], [DongSo]) 
values			( N'Nguyễn Thùy Linh', N'166638429', N'125 Lê Lợi, Q1, TP.HCM', 3500000, 1, 1),
				( N'Nguyễn Thị Kiều Trang', N'721545792', N'227 Nguyễn Huệ, Q3, TP.HCM', 5000000, 1, 1) ,
				( N'Nguyễn Xuân Thu', N'184480747', N'16 Lý Chính Thắng, Q3, TP.HCM', 1000000, 2, 1),
				( N'Nguyễn Bình Minh', N'354865924', N'12 Cao Lỗ, Q8, TP.HCM', 4000000, 1, 1),
				( N'Nguyễn Xuân Trường', N'931907060', N'67 Kha Vạn Cân, Q9, TP.HCM', 1200000, 2, 1),
				( N'Trần Hào Nam', N'367209623', N'Lê Đình Chi, Lê Minh Xuân, Bình Chánh, TP.HCM', 6000000, 1, 1) ,
				( N'Trương Mỹ Linh', N'851122372', N'25 Mỹ Bình, Tân Trụ, Tỉnh Long An', 2500000, 2, 1),
				( N'Trần Minh An', N'490622169', N'27/1 ấp Xuân Thới Đông 2, Xuân Thới Đông, Hóc Môn, TP.HCM', 3200000, 2, 1),
				(N'Lê Thanh Phong','301677613',N'16/4 An Thạnh Bến Lức Long An',15000000,3,1),
				(N'Đỗ Tuấn Kiệt','369856325',N'Đồng Nai',92000000,3,1)
go

--------------------------------------
if OBJECT_ID('TongTienGui') is not null
	drop function TongTienGui
go
create function TongTienGui
(@maso int)
returns int
as
begin
	declare @kq int
	set @kq=(select sum(SoTienGui) as SoTienGui
			from PhieuGuiTien
			where MaSoTietKiem=@maso)
	return @kq
end
go
--------------------------------
if OBJECT_ID('TongTienRut') is not null
	drop function TongTienRut
go
create function TongTienRut
(@maso int)
returns int
as
begin
	declare @kq int
	set @kq=(select sum(SoTienRut) as SoTienRut
			from PhieuRutTien
			where MaSoTietKiem=@maso)
	return @kq
end
go

--------------Thêm Quy Định 2-------------------
create trigger trg_QuyDinh2 on PhieuGuiTien
for insert,update
as
begin
	if(not exists(select*
					from inserted i join SoTietKiem s on i.MaSoTietKiem=s.MaSoTietKiem
					where s.LoaiTietKiem=3 and i.SoTienGui >=100000))
	begin
		print N'Nhập Sai Dữ Liệu'
		rollback tran
	end
	else
	begin
		if(exists(select *
					from inserted i
					where i.BiXoa=1))
		begin
			update SoTietKiem set SoDu=s.SoDu-i.SoTienGui
			from inserted i,SoTietKiem s
			where s.MaSoTietKiem=i.MaSoTietKiem
		end
		else
		begin
		update PhieuGuiTien set NgayGui=getdate()
		from inserted i,PhieuGuiTien s
		where i.MaPhieuGuiTien=s.MaPhieuGuiTien and s.MaSoTietKiem=i.MaSoTietKiem
		update SoTietKiem set SoDu=s.SoTienGuiBanDau+dbo.TongTienGui(s.MaSoTietKiem)
		from inserted i,SoTietKiem s
		where i.MaSoTietKiem=s.MaSoTietKiem
		end
	end
end
go
----------------------------------

insert PhieuGuiTien(MaSoTietKiem,TenKhachHang,SoTienGui)
values
		('5',N'Nguyễn Huỳnh Thảo Mai',350000),
		('6',N'Trần Trung Chính',750000),
		('2',N'Vương Thanh Hoàng',3250000),
		('3',N'Lâm Hoàn',150000),
		('15',N'Lê Thanh Phong',3250000)
go
--------------Thêm Quy Định 3-------------------
create trigger trg_QuyDinh3_1 on PhieuRutTien
for insert
as
begin
	if(not exists(select *
				from inserted i join SoTietKiem s on i.MaSoTietKiem=s.MaSoTietKiem
								join LoaiTietKiem l on l.MaLoaiTietKiem=s.LoaiTietKiem
				where datediff(d,s.NgayMoSo,getdate())>=15 and (((s.LoaiTietKiem=1 or s.LoaiTietKiem=2)and(i.SoTienRut=s.SoDu))or(s.LoaiTietKiem=3 and i.SoTienRut<=s.SoDu))))
			
	begin
		print N'Nhập Dữ Liệu Sai'
		rollback tran--ok
	end
	else
	begin
		update PhieuRutTien set NgayRut=getdate()
		from inserted i,PhieuRutTien s
		where i.MaSoTietKiem=s.MaSoTietKiem and i.MaPhieuRutTien=s.MaPhieuRutTien
		if(exists(select *
				from inserted i join SoTietKiem s on i.MaSoTietKiem=s.MaSoTietKiem
				where i.SoTienRut<s.SoDu))
				begin
						update SoTietKiem set SoDu=s.SoTienGuiBanDau+dbo.TongTienGui(s.MaSoTietKiem)-dbo.TongTienRut(s.MaSoTietKiem)
						from inserted i,SoTietKiem s
						where i.MaSoTietKiem=s.MaSoTietKiem
				end
			end
		end


go
create trigger trg_update on PhieuRutTien
for update
as
begin
if(exists(select *
				from inserted i
				where i.BiXoa=1))
		begin
			update SoTietKiem set SoDu=s.SoDu+p.SoTienRut
			from PhieuRutTien p,inserted i,SoTietKiem s
			where p.MaPhieuRutTien=i.MaPhieuRutTien and p.MaSoTietKiem=s.MaSoTietKiem
		end
		else
		begin
		update PhieuRutTien set NgayRut=getdate()
		from inserted i,PhieuRutTien s
		where i.MaSoTietKiem=s.MaSoTietKiem and i.MaPhieuRutTien=s.MaPhieuRutTien
		if(exists(select *
				from inserted i join SoTietKiem s on i.MaSoTietKiem=s.MaSoTietKiem
				where i.SoTienRut<s.SoDu))
				begin
						update SoTietKiem set SoDu=s.SoTienGuiBanDau+dbo.TongTienGui(s.MaSoTietKiem)-dbo.TongTienRut(s.MaSoTietKiem)
						from inserted i,SoTietKiem s
						where i.MaSoTietKiem=s.MaSoTietKiem
				end
			else if(exists(select *
							from inserted i join SoTietKiem s on s.MaSoTietKiem=i.MaSoTietKiem
							where i.SoTienRut=s.SoDu))
			begin
				update SoTietKiem set SoDu=0
						from inserted i,SoTietKiem s
						where i.MaSoTietKiem=s.MaSoTietKiem
			end
		end
end
go


---------------------------------------------PROCEDUE----------------------------------------------------------------------------
--------------LOẠI TIẾT KIỆM------------------------
--Sửa Loại Tiết Kiệm
IF OBJECT_ID('stk_SuaLoaiTietKiem','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_SuaLoaiTietKiem]
GO
CREATE PROC stk_SuaLoaiTietKiem
	@MaLoaiTietKiem int,
	@TenLoaiTietKiem nvarchar(50),
	@LaiSuat float,
	@KyHan int,
	@SoNgayDuocRut int,
	@DangDung bit,
	@DuocGuiThem bit
AS
BEGIN
		UPDATE LoaiTietKiem
		SET TenLoaiTietKiem = @TenLoaiTietKiem, LaiSuat = @LaiSuat, KyHan= @KyHan, SoNgayDuocRut = @SoNgayDuocRut, DangDung = @DangDung,  DuocGuiThem = @DuocGuiThem
		WHERE MaLoaiTietKiem = @MaLoaiTietKiem 
END

----------Xóa Loại Tiết Kiệm------------------------------
IF OBJECT_ID('stk_XoaLoaiTietKiem','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_XoaLoaiTietKiem]
GO
CREATE PROC stk_XoaLoaiTietKiem
	@MaLoaiTietKiem int
AS
BEGIN
		update LoaiTietKiem set BiXoa = 1 where MaLoaiTietKiem = @MaLoaiTietKiem
END

-------------------------------------------------
--------------SỔ TIẾT KIỆM-----------------------
-----Sửa Sổ Tiết Kiệm
go
IF OBJECT_ID('stk_SuaSoTietKiem','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_SuaSoTietKiem]
GO
CREATE PROC stk_SuaSoTietKiem
	@MaSoTietKiem int,
	@TenKhachHang nvarchar(50),
	@SoCMND nvarchar(50),
	@DiaChi nvarchar(255),
	@NgayMoSo date,
	@SoTienGuiBanDau int,
	@LoaiTietKiem int,
	@DongSo bit
AS
BEGIN
		UPDATE SoTietKiem
		SET TenKhachHang = @TenKhachHang, SoCMND = @SoCMND, DiaChi = @DiaChi, NgayMoSo = @NgayMoSo, SoTienGuiBanDau = @SoTienGuiBanDau, LoaiTietKiem = @LoaiTietKiem, DongSo = @DongSo
		WHERE MaSoTietKiem = @MaSoTietKiem
END
GO
---------------------------------------------------
-----Xóa Sổ Tiết Kiệm
IF OBJECT_ID('stk_XoaSoTietKiem','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_XoaSoTietKiem]
GO
CREATE PROC stk_XoaSoTietKiem
	@MaSoTietKiem int
AS
BEGIN
		update SoTietKiem set BiXoa = 1 where MaSoTietKiem = @MaSoTietKiem
END

Go
-----------------------------------
----------PHIỂU RÚT TIỀN-----------
--Sửa Phiếu Rút Tiền
IF OBJECT_ID('stk_SuaPhieuRutTien','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_SuaPhieuRutTien]
GO
CREATE PROC stk_SuaPhieuRutTien
	@MaPhieuRutTien int,
	@MaSoTietKiem int,
	@TenKhachHang nvarchar(50),
	@SoTienRut int,
	@NgayRut date
	
AS
BEGIN
		UPDATE PhieuRutTien
		SET MaSoTietKiem = @MaSoTietKiem, TenKhachHang = @TenKhachHang, SoTienRut = @SoTienRut, NgayRut = @NgayRut
		WHERE MaPhieuRutTien = @MaPhieuRutTien
END
go
-----------Xóa Phiếu Rút Tiền-------------------------
IF OBJECT_ID('stk_XoaPhieuRutTien','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_XoaPhieuRutTien]
GO
CREATE PROC stk_XoaPhieuRutTien
	@MaPhieuRutTien int
AS
BEGIN
		update PhieuRutTien set BiXoa = 1 where MaPhieuRutTien = @MaPhieuRutTien
END
go
----------PHIỂU GỬI TIỀN-----------
-----------Xóa Phiếu Gửi Tiền-------------------------
IF OBJECT_ID('stk_XoaPhieuGuiTien','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_XoaPhieuGuiTien]
GO
CREATE PROC stk_XoaPhieuGuiTien
	@MaPhieuGuiTien int
AS
BEGIN
		update PhieuGuiTien set BiXoa = 1 where MaPhieuGuiTien = @MaPhieuGuiTien
END
go


--Sửa Phiếu Gửi Tiền
IF OBJECT_ID('stk_SuaPhieuGuiTien','P') IS NOT NULL
	--XÓA CSDL
	DROP PROC [stk_SuaPhieuGuiTien]
GO
CREATE PROC stk_SuaPhieuGuiTien
	@MaPhieuGuiTien int,
	@MaSoTietKiem int,
	@TenKhachHang nvarchar(50),
	@SoTienGui int,
	@NgayGui date
	
AS
BEGIN
		UPDATE PhieuGuiTien
		SET MaSoTietKiem = @MaSoTietKiem, TenKhachHang = @TenKhachHang, SoTienGui = @SoTienGui, NgayGui = @NgayGui
		WHERE MaPhieuGuiTien = @MaPhieuGuiTien
END